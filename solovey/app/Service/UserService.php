<?php
/**
 * | -----------------------------
 * | Created by exp on 4/15/18/11:35 PM.
 * | Site: teslex.tech
 * | ------------------------------
 * | UserService.php
 * | ---
 */

namespace Service;


use Model\User;
use Solovey\Database\Database;
use Solovey\Database\Methods\Crud;

class UserService
{
	/**
	 * @param string $username
	 * @param array $fields
	 * @return bool|mixed
	 */
	public static function getByUsername($username, $fields = [])
	{
		$norm = Crud::normalizeByClass(User::class);
		$table = User::getTable();

		foreach ($fields as $index => $field)
			if (!in_array($field, $norm['data']['keys']))
				unset($fields[$index]);

		$fields = empty($fields) ? '*' : implode(',', $fields);

		$query = "SELECT $fields FROM $table WHERE username = ?";

		$result = Database::query($query)->data([$username])->execute()->fetch();

		return $result ? a2o($result, User::class) : false;
	}
}