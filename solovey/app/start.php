<?php
/**
 * | -----------------------------
 * | Created by exp on 4/15/18/10:47 PM.
 * | Site: teslex.tech
 * | ------------------------------
 * | start.php
 * | ---
 */

use Solovey\Database\Database;

require_once "router.php";
require_once "config.dev.php";
//require_once "config.php";


Database::init(DATABASE);

startApplication();