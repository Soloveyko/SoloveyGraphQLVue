<?php
/**
 * | -----------------------------
 * | Created by exp on 4/15/18/11:22 PM.
 * | Site: teslex.tech
 * | ------------------------------
 * | User.php
 * | ---
 */

namespace Model;


use Solovey\Database\Model\Table;

class User implements Table
{

	public $id;
	public $username;
	public $email;
	public $password;

	/**
	 * User constructor.
	 * @param $username
	 * @param $email
	 * @param $password
	 */
	public function __construct($username, $email, $password)
	{
		$this->username = $username;
		$this->email = $email;
		$this->password = $password;
	}

	/**
	 * @return mixed
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param mixed $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * @return mixed
	 */
	public function getUsername()
	{
		return $this->username;
	}

	/**
	 * @param mixed $username
	 */
	public function setUsername($username)
	{
		$this->username = $username;
	}

	/**
	 * @return mixed
	 */
	public function getEmail()
	{
		return $this->email;
	}

	/**
	 * @param mixed $email
	 */
	public function setEmail($email)
	{
		$this->email = $email;
	}

	/**
	 * @return mixed
	 */
	public function getPassword()
	{
		return $this->password;
	}

	/**
	 * @param mixed $password
	 */
	public function setPassword($password)
	{
		$this->password = $password;
	}

	/**
	 * @return string
	 */
	static function getTable()
	{
		return "users";
	}

	/**
	 * @return string
	 */
	static function getKey()
	{
		return "id";
	}
}