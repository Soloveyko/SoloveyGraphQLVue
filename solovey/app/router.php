<?php
/**
 * | -----------------------------
 * | Created by exp on 4/15/18/10:47 PM.
 * | Site: teslex.tech
 * | ------------------------------
 * | router.php
 * | ---
 */

use Solovey\Routing\Router;
use Type\QueryType;
use Youshido\GraphQL\Execution\Processor;
use Youshido\GraphQL\Schema\Schema;

Router::ANY('graphql', '/graphql', function () {
	$processor = new Processor(new Schema([
		'query' => new QueryType()
	]));

	$in = json_decode(DATA(), true);

	$payload = $in['query'];

	$processor->processPayload($payload);
	response($processor->getResponseData());
});

Router::GET('root', '/', function () {
	readfile($_SERVER['DOCUMENT_ROOT'] . '/static/index.html');
});