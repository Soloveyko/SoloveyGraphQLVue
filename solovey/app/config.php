<?php

define('DATABASE', [
	'driver' => 'pgsql',
	'host' => 'localhost',
	'port' => 5432,
	'user' => 'user',
	'name' => 'postgres',
	'password' => 'password'
]);

// enable debug
ini_set('display_errors', 'on');
error_reporting(E_ALL | ~E_WARNING | ~E_NOTICE);