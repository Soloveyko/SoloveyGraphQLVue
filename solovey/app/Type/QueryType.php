<?php
/**
 * | -----------------------------
 * | Created by exp on 4/15/18/11:25 PM.
 * | Site: teslex.tech
 * | ------------------------------
 * | QueryType.php
 * | ---
 */

namespace Type;


use Model\User;
use function r;
use Service\UserService;
use Solovey\Database\Database;
use Youshido\GraphQL\Config\Object\ObjectTypeConfig;
use Youshido\GraphQL\Execution\ResolveInfo;
use Youshido\GraphQL\Type\ListType\ListType;
use Youshido\GraphQL\Type\NonNullType;
use Youshido\GraphQL\Type\Object\AbstractObjectType;
use Youshido\GraphQL\Type\Scalar\IntType;
use Youshido\GraphQL\Type\Scalar\StringType;

class QueryType extends AbstractObjectType
{

	/**
	 * @param ObjectTypeConfig $config
	 */
	public function build($config)
	{
		$config->addFields([
			'user' => [
				'type' => new UserType(),
				'description' => 'Get user by id',
				'args' => [
					'id' => new NonNullType(new IntType())
				],
				'resolve' => function ($v, $a, ResolveInfo $i) {
					$fields = [];

					foreach ($i->getFieldASTList() as $field)
						array_push($fields, $field->getName());

					return Database::crud(User::class)->get($a['id'], $fields);
				}
			],
			'userByUsername' => [
				'type' => new UserType(),
				'description' => 'Get user by username',
				'args' => [
					'username' => new NonNullType(new StringType())
				],
				'resolve' => function ($v, $a, ResolveInfo $i) {
					$fields = [];

					foreach ($i->getFieldASTList() as $field)
						array_push($fields, $field->getName());

					return UserService::getByUsername($a['username'], $fields);
				}
			],
			'findUsersByUsername' => [
				'type' => new ListType(new UserType()),
				'description' => 'Find users by username',
				'args' => [
					'username' => new NonNullType(new StringType()),
					'page' => new IntType()
				],
				'resolve' => function ($v, $a, ResolveInfo $i) {
					$fields = [];

					foreach ($i->getFieldASTList() as $field)
						array_push($fields, $field->getName());

					$data = Database::pagination(User::class, 10, ['username{LIKE{%#%}}' => $a['username']])->page($a['page'], $fields);

					return $data;
				}
			],
			'findUsersByEmail' => [
				'type' => new ListType(new UserType()),
				'description' => 'Find users by email',
				'args' => [
					'email' => new NonNullType(new StringType()),
					'page' => new IntType()
				],
				'resolve' => function ($v, $a, ResolveInfo $i) {
					$fields = [];

					foreach ($i->getFieldASTList() as $field)
						array_push($fields, $field->getName());

					$data = Database::pagination(User::class, 10, ['email{LIKE{%#%}}' => $a['email']])->page($a['page'], $fields);

					return $data;
				}
			]
		]);
	}
}