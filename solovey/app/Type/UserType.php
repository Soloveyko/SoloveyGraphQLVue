<?php
/**
 * | -----------------------------
 * | Created by exp on 4/15/18/11:24 PM.
 * | Site: teslex.tech
 * | ------------------------------
 * | UserType.php
 * | ---
 */

namespace Type;


use Youshido\GraphQL\Config\Object\ObjectTypeConfig;
use Youshido\GraphQL\Type\Object\AbstractObjectType;
use Youshido\GraphQL\Type\Scalar\IntType;
use Youshido\GraphQL\Type\Scalar\StringType;

class UserType extends AbstractObjectType
{

	/**
	 * @param ObjectTypeConfig $config
	 */
	public function build($config)
	{
		$config->addFields(
			[
			'id' => new IntType(),
			'username' => new StringType(),
			'email' => new StringType()
		]);
	}
}