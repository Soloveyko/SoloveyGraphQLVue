#!/bin/bash

rm build.log.txt

echo "Setting Solovey.."
rm -rf dist &>> build.log.txt
mkdir dist &>> build.log.txt
mkdir dist/static &>> build.log.txt
mkdir dist/pages &>> build.log.txt
cp -a ./solovey/app ./solovey/vendor ./solovey/index.php ./solovey/pages ./solovey/static dist &>> build.log.txt

echo "
Order Deny,Allow
Deny from all
" >> ./dist/pages/.htaccess
echo "
Order Deny,Allow
Deny from all
" >> ./dist/app/.htaccess
echo "
Order Deny,Allow
Deny from all
" >> ./dist/vendor/.htaccess

echo "
RewriteEngine Off
" >> ./dist/static/.htaccess

echo "
<IfModule mod_rewrite.c>

    RewriteEngine On

    RewriteCond %{REQUEST_FILENAME} !-l
    RewriteCond %{REQUEST_FILENAME} !-f
    RewriteCond %{REQUEST_FILENAME} !-d
    RewriteRule ^(.*)$ index.php [QSA,L]

</IfModule>
" >> ./dist/.htaccess

echo "Setting Vue.."
cd vue &>> ../build.log.txt
npm run build &>> ../build.log.txt
cd dist &>> ../../build.log.txt
cp -a static/* . &>> ../../build.log.txt
rm -rf static &>> ../../build.log.txt
cd ../../ &>> build.log.txt
cp -a ./vue/dist/* ./dist/static &>> build.log.txt

echo ""
echo "Build log in file build.log.txt"
